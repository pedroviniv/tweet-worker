/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.time;

import java.time.LocalTime;

/**
 *
 * @author pafer
 */
public interface TimeCalculator {
    
    LocalTime add(LocalTime now, float interval);
}
