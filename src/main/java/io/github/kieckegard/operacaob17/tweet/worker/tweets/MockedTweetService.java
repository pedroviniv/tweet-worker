/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.springframework.stereotype.Service;

/**
 *
 * @author pafer
 */

@Service
public class MockedTweetService implements TweetService {
    
    private int[] tweetPublishingChance = new int[]{
        1,1,1,1,1,1,1,1,1,1,1,1,1,0
    };
    
    private String tweetId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public List<String> getNextTweets(int limit) throws UnavailableTweetServiceException {
        
        List<String> ids = new ArrayList<>();
        for(int i = 0; i < limit; i++) {
            ids.add(tweetId());
        }
        return ids;
    }

    @Override
    public void publish(String tweetId) throws UnavailableTweetServiceException {
        
        int size = this.tweetPublishingChance.length;
        
        Random random = new Random();
        int index = random.nextInt(size);
        
        if(this.tweetPublishingChance[index] == 0) {
            throw new UnavailableTweetServiceException("The tweet service is unavailable.");
        }
    }
    
}
