/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets.db;

import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetInterval;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author pafer
 */

@Service
public class TweetIntervalDAOJDBCImpl implements TweetIntervalDAO {

    private DataSource dataSource;
    
    @Value("${tweetInterval.batch.size}")
    private int batchSize;
    
    private static final Logger LOG = Logger.getLogger(TweetIntervalDAOJDBCImpl.class.getName());

    @Autowired
    public TweetIntervalDAOJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private void addBatch(PreparedStatement pstm, TweetInterval tweetInterval) throws SQLException {
        
        pstm.setString(1, tweetInterval.getTweetId());
        pstm.setFloat(2, tweetInterval.getInterval());
        pstm.addBatch();
    }
    
    @Override
    public void persist(Stream<TweetInterval> tweetIntervals) {
        try {
            
            String sql = "INSERT INTO tweet_interval VALUES(?,?)";
            
            Connection connection = this.dataSource.getConnection();
            PreparedStatement pstm = connection.prepareStatement(sql);
            
            Iterator<TweetInterval> iterator = tweetIntervals.iterator();
            int count = 0;
            while(iterator.hasNext()) {
                
                TweetInterval tweetInterval = iterator.next();
                
                this.addBatch(pstm, tweetInterval);
                if(count == this.batchSize || !iterator.hasNext()) {
                    pstm.executeBatch();
                    count = 0;
                }
            }
            
            pstm.close();
            connection.close();
            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public TweetInterval next() {
        
        String sql = "SELECT * FROM tweet_interval LIMIT 1";
        
        try {
            Connection connection = this.dataSource.getConnection();
            PreparedStatement pstm = connection.prepareStatement(sql);
            
            ResultSet resultSet = pstm.executeQuery();
            
            boolean next = resultSet.next();
            
            if(!next) {
                throw new TweetIntervalNotFoundException("There's no next tweet in the database.");
            }
            
            String tweetId = resultSet.getString("id");
            float interval = resultSet.getFloat("interval");
            
            pstm.close();
            connection.close();
            
            return new TweetInterval(tweetId, interval);
            
        } catch (SQLException ex) {
            throw new RuntimeException("Couldn't get the next tweet.", ex);
        }
    }

    @Override
    public void remove(String id) {
        
        String sql = "DELETE FROM tweet_interval WHERE id = ?";
        
        try {
            Connection connection = this.dataSource.getConnection();
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, id);
            int result = pstm.executeUpdate();
            
            pstm.close();
            connection.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(TweetIntervalDAOJDBCImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Couldn't remove the tweet " + id, ex);
        }
    }
    
    
    
}
