/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets;

import java.util.List;

/**
 *
 * @author pafer
 */
public interface TweetService {
    
    public List<String> getNextTweets(int limit) throws UnavailableTweetServiceException;
    public void publish(String tweetId) throws UnavailableTweetServiceException;
}
