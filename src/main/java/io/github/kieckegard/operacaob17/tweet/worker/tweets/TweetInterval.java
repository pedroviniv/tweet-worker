/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author pafer
 */
public class TweetInterval implements Serializable {

    private String tweetId;
    private float interval;

    public TweetInterval(String tweetId, float interval) {
        this.tweetId = tweetId;
        this.interval = interval;
    }

    protected TweetInterval() {
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public float getInterval() {
        return interval;
    }

    public void setInterval(float interval) {

        this.interval = interval;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.tweetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TweetInterval other = (TweetInterval) obj;
        if (!Objects.equals(this.tweetId, other.tweetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TweetInterval{" + "tweetId=" + tweetId + ", interval=" + interval + '}';
    }
}
