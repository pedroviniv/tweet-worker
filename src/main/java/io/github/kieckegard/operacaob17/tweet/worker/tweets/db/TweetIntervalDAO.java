/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets.db;

import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetInterval;
import java.util.stream.Stream;

/**
 *
 * @author pafer
 */
public interface TweetIntervalDAO {
    
    void persist(Stream<TweetInterval> tweetIntervals);
    TweetInterval next();
    void remove(String id);
}
