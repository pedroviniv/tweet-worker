/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.time;

import java.time.LocalTime;
import org.springframework.stereotype.Component;

/**
 *
 * @author pafer
 * 
 * An util component that adds an interval (seconds like: 0,0231) to a LocalTime.
 */
@Component
public class DefaultTimeCalculator implements TimeCalculator {

    /**
     * Converts the 
     * @param now
     * @param secondInterval
     * @return 
     */
    @Override
    public LocalTime add(LocalTime now, float secondInterval) {

        long nanoseconds = (long) (secondInterval * 60f * 1000f * 1000f * 1000f);
        System.out.printf("interval: %f, nanoseconds: %d ", secondInterval, nanoseconds);
        return now.plusNanos(nanoseconds);
    }
    
    
}
