/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker;

import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetInterval;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetService;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.UnavailableTweetServiceException;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.db.TweetIntervalDAO;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pafer
 */
public class TweetPublishmentHandler extends TimerTask {
    
    private TweetService tweetService;
    private TweetInterval tweetInterval;
    private TweetIntervalDAO tweetIntervalDAO;
    
    private static final Logger LOG = Logger
            .getLogger(TweetPublishmentHandler.class.getName());

    public TweetPublishmentHandler(TweetService tweetService, 
            TweetInterval tweetInterval, 
            TweetIntervalDAO tweetIntervalDAO) {
        
        this.tweetService = tweetService;
        this.tweetInterval = tweetInterval;
        this.tweetIntervalDAO = tweetIntervalDAO;
    }

    @Override
    public void run() {
        
        String tweetId = this.tweetInterval.getTweetId();
        
        LOG.log(Level.INFO, "tweet[{0}]: publishing...", tweetId);
        try {
            //publishing the tweet
            this.tweetService.publish(tweetId);
            LOG.log(Level.INFO, "tweet[{0}]: publicado, removendo do DB...",
                    tweetId);
            //If all went ok, the tweetId entry in the db will be removed.
            this.tweetIntervalDAO.remove(tweetId);
            LOG.log(Level.INFO, "tweet[{0}]: Removido.", tweetId);
            
        } catch (UnavailableTweetServiceException ex) {
            LOG.log(Level.INFO, "tweet[{0}]: Falha ao publicar, serviço"
                    + " indisponível.", tweetId);
        }
    }
    
}
