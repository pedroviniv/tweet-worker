/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets.db;

import javax.sql.DataSource;
import org.postgresql.ds.PGPoolingDataSource;
import org.postgresql.jdbc3.Jdbc3PoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author pafer
 */

@Configuration
public class DatabaseConfig {
    
    private DatabaseSettings settings;

    @Autowired
    public DatabaseConfig(DatabaseSettings settings) {
        this.settings = settings;
    }
    
    @Bean
    public DataSource dataSource() {
        
        PGPoolingDataSource dataSource = new Jdbc3PoolingDataSource();
        dataSource.setUser(this.settings.getUsername());
        dataSource.setUrl(this.settings.getUrl());
        dataSource.setPassword(this.settings.getPassword());
        dataSource.setMaxConnections(14);
        
        return dataSource;
    }
    
}
