/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets;

import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pafer
 */

@Component
public class TweetIntervalMapper implements Function<String, TweetInterval> {

    private TweetIntervalRandomizer intervalGenerator;

    @Autowired
    public TweetIntervalMapper(TweetIntervalRandomizer intervalGenerator) {
        this.intervalGenerator = intervalGenerator;
    }
    
    @Override
    public TweetInterval apply(String t) {
        
        float publishInterval = this.intervalGenerator.generate();
        
        TweetInterval tweetInterval = new TweetInterval();
        tweetInterval.setTweetId(t);
        tweetInterval.setInterval(publishInterval);
        
        return tweetInterval;
    }

    
}
