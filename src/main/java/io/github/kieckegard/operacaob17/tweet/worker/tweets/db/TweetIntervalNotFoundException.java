/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets.db;

/**
 *
 * @author pafer
 */
public class TweetIntervalNotFoundException extends RuntimeException {

    public TweetIntervalNotFoundException(String message) {
        super(message);
    }

    public TweetIntervalNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TweetIntervalNotFoundException(Throwable cause) {
        super(cause);
    }
}
