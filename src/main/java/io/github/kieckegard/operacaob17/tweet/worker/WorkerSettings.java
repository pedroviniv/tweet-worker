/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author pafer
 */

@Component
public class WorkerSettings {
    
    @Value("${worker.interval.minutes}")
    private int minutesInterval;
    @Value("${worker.tweets.limit}")
    private int tweetsPerInterval;

    public WorkerSettings(int minutesInterval, int tweetsPerInterval) {
        this.minutesInterval = minutesInterval;
        this.tweetsPerInterval = tweetsPerInterval;
    }

    public WorkerSettings() {
    }

    public int getMinutesInterval() {
        return minutesInterval;
    }

    public void setMinutesInterval(int minutesInterval) {
        this.minutesInterval = minutesInterval;
    }

    public int getTweetsPerInterval() {
        return tweetsPerInterval;
    }

    public void setTweetsPerInterval(int tweetsPerInterval) {
        this.tweetsPerInterval = tweetsPerInterval;
    }
    
    public float getIntervalPerTweet() {
        return ((float) this.minutesInterval / (float) this.tweetsPerInterval);
    }

    @Override
    public String toString() {
        return "WorkerSettings{" + "minutesInterval=" + minutesInterval + ", tweetsPerInterval=" + tweetsPerInterval + '}';
    }
}
