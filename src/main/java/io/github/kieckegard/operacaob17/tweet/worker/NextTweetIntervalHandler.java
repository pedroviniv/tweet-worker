/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker;

import io.github.kieckegard.operacaob17.tweet.worker.time.TimeCalculator;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetInterval;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetService;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.db.TweetIntervalDAO;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.db.TweetIntervalNotFoundException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pafer
 */
public class NextTweetIntervalHandler extends TimerTask {

    private TweetIntervalDAO tweetIntervalDAO;
    private TimeCalculator timeCalculator;
    private TweetService tweetService;
    private static final Logger LOG = Logger.getLogger(NextTweetIntervalHandler.class.getName());

    public NextTweetIntervalHandler(TweetIntervalDAO tweetIntervalDAO, TimeCalculator timeCalculator, TweetService tweetService) {
        this.tweetIntervalDAO = tweetIntervalDAO;
        this.timeCalculator = timeCalculator;
        this.tweetService = tweetService;
    }

    @Override
    public void run() {

        try {

            TweetInterval tweetInterval = this.tweetIntervalDAO.next();

            LocalTime now = LocalTime.now();
            LocalTime tweetPublishTime = this.timeCalculator.add(now,
                    tweetInterval.getInterval());

            LOG.log(Level.INFO, "tweet[{0}]: current[{1}], publishTime[{2}]",
                    new Object[]{tweetInterval.getTweetId(), now, tweetPublishTime});

            long delay = ChronoUnit.MILLIS.between(now, tweetPublishTime);

            TweetPublishmentHandler publisherTask
                    = new TweetPublishmentHandler(this.tweetService, 
                            tweetInterval, this.tweetIntervalDAO);

            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.schedule(publisherTask, delay, TimeUnit.MILLISECONDS);

        } catch (TweetIntervalNotFoundException ex) {
            LOG.log(Level.INFO, "There's no next tweet to handle.", ex);
        }
    }

}
