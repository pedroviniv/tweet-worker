/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker;

import io.github.kieckegard.operacaob17.tweet.worker.time.TimeCalculator;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetInterval;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetIntervalMapper;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.TweetService;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.UnavailableTweetServiceException;
import io.github.kieckegard.operacaob17.tweet.worker.tweets.db.TweetIntervalDAO;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author pafer
 */
@Component
public class Worker {

    private WorkerSettings settings;
    
    private TweetService tweetService;
    private TweetIntervalDAO tweetIntervalDAO;
    private TimeCalculator timeCalculator;
    private TweetIntervalMapper tweetIntervalMapper;
    
    private static final Logger LOG = Logger.getLogger(Worker.class.getName());

    private boolean subSchedulingStarted;

    @Autowired
    public Worker(TweetService tweetService, WorkerSettings settings,
            TweetIntervalDAO tweetIntervalDAO, TimeCalculator timeCalculator,
            TweetIntervalMapper tweetIntervalMapper) {
        this.tweetService = tweetService;
        this.settings = settings;
        this.tweetIntervalDAO = tweetIntervalDAO;
        this.timeCalculator = timeCalculator;
        this.tweetIntervalMapper = tweetIntervalMapper;

        this.subSchedulingStarted = false;
    }

    /**
     * General scheduler. 
     * <ul>
     *  <li>gets the next tweets ids from tweetService</li>
     *  <li>calculate the interval to each tweetId</li>
     *  <li>persists each tweetid with it's interval in the database</li>
     *  <li>start sub scheduling, that will execute a task each worker.minutes.interval/worker.tweets.size</li>
     * </ul>
     */
    @Scheduled(cron = "${worker.cronExpression}")
    public void calculateNextTweetsIntervals() {

        try {
            List<String> tweetsIds = this.tweetService
                    .getNextTweets(this.settings.getTweetsPerInterval());

            Stream<TweetInterval> tweetIntervalStream = tweetsIds
                    .stream()
                        .map(this.tweetIntervalMapper);

            this.tweetIntervalDAO.persist(tweetIntervalStream);
            
            this.startSubScheduling();

        } catch (UnavailableTweetServiceException ex) {
            LOG.log(Level.INFO, "Não foi possível buscar os proximos tweets do webservice.");
        }
    }

    /**
     * Starts subscheduler, converts the tweet interval limit
     */
    private void startSubScheduling() {
        
        if (!this.subSchedulingStarted) {

            Timer timer = new Timer();

            NextTweetIntervalHandler intervalHandler
                    = new NextTweetIntervalHandler(this.tweetIntervalDAO,
                            this.timeCalculator, this.tweetService);
            
            long milli = (long) (this.settings.getIntervalPerTweet() * 60f * 1000f);
            System.out.println("static scheduling[milli]: " + milli);
            
            timer.schedule(intervalHandler, new Date(), milli);
            this.subSchedulingStarted = true;
        }
    }

}
