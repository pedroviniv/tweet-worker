/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.operacaob17.tweet.worker.tweets;

import io.github.kieckegard.operacaob17.tweet.worker.WorkerSettings;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pafer
 */

@Component
public class TweetIntervalRandomizer {
    
    private WorkerSettings workerSettings;

    @Autowired
    public TweetIntervalRandomizer(WorkerSettings workerSettings) {
        this.workerSettings = workerSettings;
    }
    
    public float generate() {
        
        Random random = new Random();
        float randomFloat = random.nextFloat();
        float randomizedInterval = randomFloat * this.workerSettings.getIntervalPerTweet();
        System.out.printf("random float: %f * tweetInterval: %f = randomized interval: %f\n", randomFloat, this.workerSettings.getIntervalPerTweet(), randomizedInterval);
        
        return randomizedInterval;
    }
}
